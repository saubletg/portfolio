import styles from "./Article_fourth.module.scss";
import Link from "next/link";
import { useRef, useEffect } from "react";

import {gsap} from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

import {BsArrowRightCircle} from "react-icons/bs";
import { IconContext } from "react-icons";

export default function Article_two() {
    gsap.registerPlugin(ScrollTrigger);
    
    const panel = useRef();
    
    useEffect(()=>{
        gsap.fromTo(".Article_fourth_project_panel__4395M", {
            x: 0
        },{
            x: "100%",
            duration:1,
            scrollTrigger : {
                start: 'top bottom',
                trigger: ".Article_fourth_project_container__d5cjz",
            }
        });
    }, [panel])

    return (
        <article className={styles.project_container}>
            <div className={styles.project_box}>
                <Link href="/">
                    <a>
                        <div className={styles.project_img}>
                            <div className={styles.project_panel}ref={panel}>
                            </div>
                        </div>
                                
                    </a>
                </Link>
            </div>
            <div className={styles.project_infos}>
                <div className={styles.project_infos_inner}>
                    <h3 className={styles.project_title}>Fisheye</h3>
                        <p className={styles.project_infos_text}>
                            <span className={styles.project_infos_date}>(2021-)</span>
                        Web service for hiring freelance photographers. 
                        </p>
                    <Link href="/">
                        <a className={styles.wrap_link}>
                            <IconContext.Provider value={{ color: "white", size: "2em"}}>
                                <BsArrowRightCircle/>
                            </IconContext.Provider>
                            <div className={styles.text_link}>View Project</div>
                        </a>
                    </Link>
                </div>
            </div>
        </article>
    )
}