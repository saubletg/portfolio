import styles from "./Projects.module.scss";
import Link from "next/link";
import { useRef, useEffect } from "react";

import {gsap} from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

import {BsArrowRightCircle} from "react-icons/bs";
import { IconContext } from "react-icons";

import Article from "../Article/Article"
import Article_two from "../Article_two/Article_two";
import Article_three from "../Article_three/Article_three"
import Article_fourth from "../Article_fourth/Article_fourth"

export default function Projects() {
    gsap.registerPlugin(ScrollTrigger);

    const loader = useRef();
    const project = useRef();

    useEffect(()=>{
        gsap.fromTo(".Projects_st__zQc5M", {
            autoAlpha: 0,
            y: 100
        },{
            autoAlpha: 1,
            y: 0,
            scrollTrigger : {
                start: 'top bottom',
                trigger: ".Projects_box__0Cphi",
            }
        });
    }, [project])

    useEffect(()=>{
        var tl = gsap.timeline({
            scrollTrigger : {
                start: 'top bottom',
                trigger: ".Projects_st_border__0qeWG",
            }
        });
        tl.to(".Projects_st_loader__Fq2y9", {x: "-100%", autoAlpha: 0});
        tl.to(".Projects_st_loader__Fq2y9", {x: 0, duration: 0.5, autoAlpha: 1, width:"100%"});
        tl.to(".Projects_st_loader__Fq2y9", {x: "100%", duration: 0.5, autoAlpha: 0});
    }, [loader])

    return (
        <section className={styles.projects_wrap}>
            <div className={styles.st_container} >
                <h2 className={styles.box} >
                    <span className={styles.st} ref={project}>Projects</span>
                </h2>
                <div className={styles.st_border}>
                    <div className={styles.st_loader} ref={loader}></div>
                </div>
            </div>
            <div className={styles.project_grid}>
                <Article/>
                <Article_two/>
                <Article_three/>
                <div className={styles.link_container}>
                    <span className={styles.link_text}>
                        More Projects on
                    </span>
                    <Link href="https://gitlab.com/saubletg">
                        <a className={styles.link}>
                             GitLab
                        </a>
                    </Link>
                </div>
            </div>
            
        </section>
    )
}