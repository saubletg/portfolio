import Link from "next/link"
import styles from "./Header.module.scss";

export default function Header() {
    return (
      <header>
          <div >
            <span className={styles.link}>
              <Link href="/">
                  <a>Gildas Saublet</a>
              </Link>
            </span>
            <ul>
                <li className={styles.link}>
                  <Link href="/">
                      <a>Work</a>
                  </Link>
                </li>
                <li className={styles.link}>
                  <Link href="/about">
                      <a>About</a>
                  </Link>
                </li>
            </ul>
          </div> 
      </header>
    )
  }