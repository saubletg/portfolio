import styles from "./Contact.module.scss";
import Link from "next/link";

export default function Contact() {
    return (
        <div className={styles.contact}>
            <Link href="mailto:saubletg@gmail.com" ><a className={styles.link}>Email</a></Link>
            <Link href="https://www.linkedin.com/in/gildas-saublet"><a className={styles.link}>Linkedin</a></Link>
        </div>
    )
}

