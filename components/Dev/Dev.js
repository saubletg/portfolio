import styles from "./Dev.module.scss";
import { useRef, useEffect } from "react";
import Image from "next/image";
import mypic from '../../public/image/dev.png';
import {gsap} from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

export default function Dev() {
    gsap.registerPlugin(ScrollTrigger);

    const loader = useRef();
    const panel = useRef();

    useEffect(()=>{
        var tl = gsap.timeline({
            scrollTrigger : {
                start: 'top bottom',
                trigger: ".Dev_dev_border__eh_BJ",
            }
        });
        tl.to(".Dev_dev_loader__OYgkt", {x: "-100%", autoAlpha: 0});
        tl.to(".Dev_dev_loader__OYgkt", {x: 0, duration: 0.5, autoAlpha: 1, width:"100%"});
        tl.to(".Dev_dev_loader__OYgkt", {x: "100%", duration: 0.5, autoAlpha: 0});
    }, [loader])

    useEffect(()=> {
        let proxy = { skew: 0 },
    skewSetter = gsap.quickSetter(".Dev_dev_image_wrapper__wsynP", "skewY", "deg"), 
    clamp = gsap.utils.clamp(-20, 20); 

    ScrollTrigger.create({
    onUpdate: (self) => {
        let skew = clamp(self.getVelocity() / -300);
        if (Math.abs(skew) > Math.abs(proxy.skew)) {
        proxy.skew = skew;
        gsap.to(proxy, {skew: 0, duration: 0.8, ease: "power3", overwrite: true, onUpdate: () => skewSetter(proxy.skew)});
        }
    }
    });

    gsap.set(".Dev_dev_image_wrapper__wsynP", {transformOrigin: "bottom", force3D: true});
    }, [panel])

    return (
        <section className={styles.dev_wrap}>
            <div className={styles.dev_container} >
                <div className={styles.dev_border}>
                    <div className={styles.dev_loader} ref={loader}></div>
                </div>
            </div>
            
            <div className={styles.dev_content_container}>
                <div className={styles.dev_content}>
                    <p className={styles.dev_sub}>(01)</p>
                    <h2 className={styles.dev_title}>Developpement</h2>
                    <p className={styles.dev_text}>With a designers eye for details, I'm passioned about implementing designs into highly interactive experiences by paying close attention to details, animations and performance.</p>
                </div>
                <div className={styles.dev_image_wrapper} ref={panel}>
                    <Image
                    src={mypic}
                    alt="Picture of Design"
                    layout='responsive'
                    objectFit='contain'
                    className={styles.dev_img}
                    />
                </div>
            </div>
        </section>
    )
}