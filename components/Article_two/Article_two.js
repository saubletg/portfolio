import styles from "./Article_two.module.scss";
import Link from "next/link";
import { useRef, useEffect } from "react";
import Image from "next/image";
import mypic from '../../public/image/ideas2.jpg';

import {gsap} from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

export default function Article_two() {
    gsap.registerPlugin(ScrollTrigger);
    
    const panel = useRef();
    const icon = useRef();
    
    useEffect(()=>{
        gsap.fromTo(".Article_two_project_img__F66aT", {
            autoAlpha: 0,
            clipPath: "polygon(0 0, 0% 0, 0% 100%, 0 100%)"
        },{
            autoAlpha: 1,
            duration: 1,
            clipPath: "polygon(0 0, 100% 0, 100% 100%, 0 100%)",
            scrollTrigger : {
                start: 'top bottom',
                trigger: ".Article_two_project_container__RA71y",
            }
        });
    }, [panel])

    useEffect(()=>{
        var tl = gsap.timeline({
            scrollTrigger : {
                start: 'top bottom',
                trigger: ".Article_two_svg__dSbsB",
            }
        });
        tl.to(".Article_two_circle__nTKCb", {strokeDasharray: 360, duration: 1, autoAlpha: 1});
        tl.to(".Article_two_arrow__7dE21", {transform: "matrix(1,0,0,1,0,0)", autoAlpha: 1, duration: 1});
    }, [icon])

    useEffect(()=> {
        let proxy = { skew: 0 },
    skewSetter = gsap.quickSetter(".Article_two_project_panel__d0JeJ", "skewY", "deg"), 
    clamp = gsap.utils.clamp(-20, 20); 

    ScrollTrigger.create({
    onUpdate: (self) => {
        let skew = clamp(self.getVelocity() / -300);
        if (Math.abs(skew) > Math.abs(proxy.skew)) {
        proxy.skew = skew;
        gsap.to(proxy, {skew: 0, duration: 0.8, ease: "power3", overwrite: true, onUpdate: () => skewSetter(proxy.skew)});
        }
    }
    });

    gsap.set(".Article_two_project_panel__d0JeJ", {transformOrigin: "bottom", force3D: true});
    }, [panel])

    return (
        <article className={styles.project_container}>
            <div className={styles.project_box}>
                <Link href="https://ideas-blog-nextjs.vercel.app/">
                    <a className={styles.project_panel} ref={panel}>
                        <Image
                        src={mypic}
                        alt="Picture of fisheye"
                        layout="intrinsic"
                        className={styles.project_img}
                        />         
                    </a>
                </Link>
                <div className={styles.project_infos}>
                    <div className={styles.project_infos_inner}>
                        <h3 className={styles.project_title}>Ideas</h3>
                            <p className={styles.project_infos_text}>
                                <span className={styles.project_infos_date}>(2022-)</span>
                            Open platform to write, read and connect around webdesign. 
                            </p>
                        <Link href="https://ideas-blog-nextjs.vercel.app/">
                            <a className={styles.wrap_link}>
                                <svg width={34} height={34} viewBox="0 0 84 84" fill="none" ref={icon} className={styles.svg}>
                                    <circle cx="42" cy="42" r="41" className={styles.circle}></circle>
                                    <path d="M42.9456 52.9577L44.9879 55L58.0272 41.9607L45.0665 29L43.0242 31.0423L52.6073 40.6254H27V43.4532H52.4502L42.9456 52.9577Z"
                                    fill="white" className={styles.arrow}></path>
                                </svg>
                                <div className={styles.text_link}>View Project</div>
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
            
        </article>
    )
}