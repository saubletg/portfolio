import styles from "./Main_About.module.scss";
import Contact from "../Contact/Contact";
import Image from 'next/image';
import mypic from "../../public/image/newblue4.png"

export default function Main_About() {
    return (
        <div className={styles.container}>       
            <h1 className={styles.title}>
                <span className={styles.title_box_first}>
                    <span className={styles.title_first}>About</span>
                </span>
            </h1>
            <div className={styles.img_container}>
                
                <div className={styles.about_content}>
                    <p className={styles.about_text}>
                    I am a developer, passionated about creating digital experiences with focus on motion and interaction.
                    </p>
                    <div className={styles.contact_content}>
                        <Contact/>
                    </div>
                </div>

                <div className={styles.img_wrapper}>
                    <Image
                    src={mypic}
                    alt="Picture of the author"
                    layout="intrinsic"
                    className={styles.img_box}/> 
                </div>
            </div>
        </div>
    )
}