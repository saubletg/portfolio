import styles from "./Main.module.scss";
import Link from "next/link";
import Image from 'next/image'
import Projects from "../Projects/Projects";
import fisheye from '../../public/image/fisheyeHeroFilter.jpg';
import ideas from '../../public/image/web-design.jpeg';
import minimalist from '../../public/image/minimalist2.jpg';

import {gsap} from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { useRef, useEffect } from "react";

export default function Main() {

    gsap.registerPlugin(ScrollTrigger);

    const icon = useRef();

    useEffect(()=>{
        var tl = gsap.timeline({
            scrollTrigger : {
                start: 'top bottom',
                trigger: ".Main_svg__d4P4x",
            }
        });
        tl.to(".Main_circle__jtbpZ", {strokeDasharray: 360, duration: 1, autoAlpha: 1});
        tl.to(".Main_arrow__A1MZM", {transform: "matrix(1,0,0,1,0,0)", duration: 1, autoAlpha: 1});
    }, [icon])

    return (
        <div className={styles.wrap}>
            <section className={styles.hero}>
                <div className={styles.container}>
                    
                    <h1 className={styles.title}>
                        <span className={styles.title_box_first}>
                            <span className={styles.title_first}>Creative</span>
                            <span className={styles.img_first_box}>
                                <Image
                                src={fisheye}
                                alt="Screenshot of fisheye"
                                className={styles.img_first}
                                layout={"responsive"}
                                />
                            </span>   
                        </span>
                        <span className={styles.title_box_second}>
                            <span className={styles.img_second_box}>
                            <Image
                            src={ideas}
                            alt="Screenshot of ideas"
                            className={styles.img_second}
                            layout={"responsive"}
                            />
                            </span> 
                            <span className={styles.title_second}>Front-end</span>
                        </span>
                        <span className={styles.title_box_third}>
                            <span className={styles.title_third}>Developper</span>
                            <span className={styles.img_third_box}>
                            <Image
                            src={minimalist}
                            alt="Screenshot of ideas"
                            className={styles.img_third}
                            layout={"responsive"}
                            />
                            </span> 
                        </span>
                    </h1>
                </div>
            </section>
            <section className={styles.text_container}>
                <div className={styles.text_wrap}>
                    <h2 className={styles.subtitle}></h2>
                    <p className={styles.text}>
                        I am a developer, passionated about creating digital experiences with focus on motion and interaction.
                    </p>
                </div>
                <Link href="/about">
                    <a className={styles.wrap_link}>
                        <svg width={34} height={34} viewBox="0 0 84 84" fill="none" ref={icon} className={styles.svg} xmlns="http://www.w3.org/2000/svg">
                            <circle cx="42" cy="42" r="41" className={styles.circle}></circle>
                            <path d="M42.9456 52.9577L44.9879 55L58.0272 41.9607L45.0665 29L43.0242 31.0423L52.6073 40.6254H27V43.4532H52.4502L42.9456 52.9577Z"
                            fill="white" className={styles.arrow}></path>
                        </svg>
                        <div className={styles.text_link}>Read More</div>
                    </a>
                </Link>
            </section>
            <Projects/>
        </div>
    )
  }