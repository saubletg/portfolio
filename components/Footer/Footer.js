import styles from "./Footer.module.scss";
import Link from "next/link";

import { useRef, useEffect } from "react";

import {gsap} from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

import Contact from "../Contact/Contact";

export default function Footer() {
    gsap.registerPlugin(ScrollTrigger);

    const footer = useRef();

    useEffect(()=>{
        var tl = gsap.timeline({
            scrollTrigger : {
                start: 'top bottom',
                trigger: ".Footer_footer_border__INupA",
            }
        });
        tl.to(".Footer_footer_loader__NMmlE", {x: "-100%", autoAlpha: 0});
        tl.to(".Footer_footer_loader__NMmlE", {x: 0, duration: 0.5, autoAlpha: 1, width:"100%"});
        tl.to(".Footer_footer_loader__NMmlE", {x: "100%", duration: 0.5, autoAlpha: 0});
    }, [footer])

    return (
        <div className={styles.footer_wrap}>
            <div className={styles.footer_border}>
            <div className={styles.footer_loader} ref={footer}></div>
            </div>
            <div className={styles.footer_inner}>
                <div className={styles.footer_credit}></div>
                <Contact/>
            </div>
        </div>
    )
}