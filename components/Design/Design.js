import styles from "./Design.module.scss";
import { useRef, useEffect } from "react";

import {gsap} from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import Image from "next/image";
import mypic from '../../public/image/design.png';
export default function Dev() {
    gsap.registerPlugin(ScrollTrigger);

    const loader = useRef();
    const panel = useRef();

    useEffect(()=>{
        var tl = gsap.timeline({
            scrollTrigger : {
                start: 'top bottom',
                trigger: ".Design_dev_border__UtGTc",
            }
        });
        tl.to(".Design_dev_loader__qOcTP", {x: "-100%", autoAlpha: 0});
        tl.to(".Design_dev_loader__qOcTP", {x: 0, duration: 0.5, autoAlpha: 1, width:"100%"});
        tl.to(".Design_dev_loader__qOcTP", {x: "100%", duration: 0.5, autoAlpha: 0});
    }, [loader])

    useEffect(()=> {
        let proxy = { skew: 0 },
    skewSetter = gsap.quickSetter(".Design_dev_image_wrapper__T0SVn", "skewY", "deg"), 
    clamp = gsap.utils.clamp(-20, 20); 

    ScrollTrigger.create({
    onUpdate: (self) => {
        let skew = clamp(self.getVelocity() / -300);
        if (Math.abs(skew) > Math.abs(proxy.skew)) {
        proxy.skew = skew;
        gsap.to(proxy, {skew: 0, duration: 0.8, ease: "power3", overwrite: true, onUpdate: () => skewSetter(proxy.skew)});
        }
    }
    });

    gsap.set(".Design_dev_image_wrapper__T0SVn", {transformOrigin: "bottom", force3D: true});
    }, [panel])

    return (
        <section className={styles.dev_wrap}>
            <div className={styles.dev_container} >
                <div className={styles.dev_border}>
                    <div className={styles.dev_loader} ref={loader}></div>
                </div>
            </div>
            <div className={styles.dev_content_container}>
                <div className={styles.dev_content}>
                    <p className={styles.dev_sub}>(02)</p>
                    <h2 className={styles.dev_title}>Design</h2>
                    <p className={styles.dev_text}>My creative journey started with Graphic Design. To code my own designs I learned how to code and has since then worked hard to stay relevant in both fields.</p>
                </div>
                <div className={styles.dev_image_wrapper} ref={panel}>
                    <Image
                    src={mypic}
                    alt="Picture of Design"
                    layout='responsive'
                    objectFit='contain'
                    className={styles.dev_img}
                    />
                </div>
            </div>
        </section>
    )
}