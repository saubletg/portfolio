import Footer from "../components/Footer/Footer"
import Header from "../components/Header/Header"
import Main_About from "../components/Main_About/Main_About"
import Dev from "../components/Dev/Dev"
import Design from "../components/Design/Design"

export default function About() {

  return (
    <>
    <Header/>
    <main>
        <Main_About/>
    </main>
    <Dev/>
    <Design/>
    <footer>
      <Footer/>
    </footer>
    </>
  )
}